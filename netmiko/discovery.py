#!/usr/bin/env python
from __future__ import print_function, unicode_literals

# Netmiko is the same as ConnectHandler
from netmiko import Netmiko
from getpass import getpass

my_device1 = {
    "host": "192.168.10.1",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

my_device2 = {
    "host": "192.168.10.2",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

my_device3 = {
    "host": "192.168.10.101",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

my_device4 = {
    "host": "192.168.10.102",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

my_device5 = {
    "host": "192.168.250.2",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

devices = [my_device1, my_device2, my_device3, my_device4, my_device5]

for device in devices:
	net_connect = Netmiko(**device)
	# Ensure in enable mode
	net_connect.enable()
	output = net_connect.send_command_expect('show run') # perfect for commands with large outputs like 'show run'
	print (output) # print new config
	#net_connect.send_config_set('spann mode pv')
	prompt = net_connect.find_prompt() + '.txt'
	print('writing to {}'.format(prompt))
	with open(prompt, 'w') as f:
		f.write(output)  # Python 3.x
	net_connect.send_command('wr')


net_connect.disconnect()