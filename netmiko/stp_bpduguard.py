#!/usr/bin/env python
from __future__ import print_function, unicode_literals

# Netmiko is the same as ConnectHandler
from netmiko import Netmiko
from getpass import getpass
import sys

ro1 = {
    "host": "192.168.10.1",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

ro2 = {
    "host": "192.168.10.2",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

sw1= {
    "host": "192.168.10.101",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

sw2 = {
    "host": "192.168.10.102",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

border = {
    "host": "192.168.250.2",
	"secret": "cisco",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

switches = [sw1]
router = [ro1, ro2]
mode = ''
if len(sys.argv) > 1:
	mode = sys.argv[1]

print(mode)	

for device in router:
	net_connect = Netmiko(**device)
	net_connect.enable()
	root_guard_fa8 = ['int fa 2/0/8', 'spann guard root']
	root_guard_fa16 = ['int fa 2/0/16', 'spann guard root']
	root_guard_fa24 = ['int fa 2/0/24', 'spann guard root']
	no_root_guard_fa8 = ['int fa 2/0/8', 'no spann guard root']
	no_root_guard_fa16 = ['int fa 2/0/16', 'no spann guard root']
	no_root_guard_fa24 = ['int fa 2/0/24', 'no spann guard root']
	
	root_guards = [root_guard_fa8, root_guard_fa16, root_guard_fa24]
	no_root_guards = [no_root_guard_fa8, no_root_guard_fa16, no_root_guard_fa24]
	
	
	if (mode == 'undo'):
		for commands in no_root_guards:
			output = net_connect.send_config_set(commands)
			print(output)
		
	else:
		for commands in root_guards:
			output = net_connect.send_config_set(commands)
			print(output)
			
	net_connect.disconnect()
			
			
for device in switches:
	net_connect = Netmiko(**device)
	# Ensure in enable mode
	net_connect.enable()
	bpdu_guard = ['int fa 0/1', 'spann bpduguard enable']
	undo_bpdu_guard = ['int fa 0/1', 'no spann bpduguard enable']
	
	if (mode == 'undo'):
		output = net_connect.send_config_set(undo_bpdu_guard)
		
	else:
		output = net_connect.send_config_set(bpdu_guard)
	print(output)
	net_connect.disconnect()
	


